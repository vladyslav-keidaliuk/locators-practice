﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace LocatorsTests
{
    internal class Locators
    {
        private static Dictionary<string, By> _headerElements = new Dictionary<string, By>()
        {
            { "Logo", By.CssSelector("body > app-root  app-logo > div > div")},
            { "BurgerMenu", By.CssSelector("body > app-root  div:nth-child(1) > app-burger-button > div")},
            { "AllNews", By.CssSelector("body > app-root app-menu li:nth-child(1) > a") },
            { "UkraineNews", By.CssSelector("app-root > app-default-layout header  app-menu  li:nth-child(2) > a") },
            { "MainNews", By.CssSelector("body > app-root  app-menu > nav > ul > li:nth-child(3) > a") },
            { "Fund24", By.CssSelector("body > app-root  app-menu > nav > ul > li:nth-child(4) > a") },
            { "SearchIcon", By.CssSelector("body > app-root app-search > div") },
            { "WatchOnline", By.CssSelector("body > app-root  app-online-button > a") },
        };

        private static Dictionary<string, By> AllCategoriesOfNews = new Dictionary<string, By>()
        {

            { "MoreLinks", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[2]") },

            { "Economy", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[1]")},
            { "Sports", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[2]")},
            { "Business", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[3]")},
            { "Health", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[4]")},
            { "Showbiz", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[5]")},
            { "Education", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[6]")},
            { "Abroad", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[7]")},
            { "Finance", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[8]")},
            { "RealEstate", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[9]")},
            { "Delicious", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/nav/a[10]")},

            { "Techno", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[11]")},
            { "Auto", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[12]")},
            { "Useful", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[13]")},
            { "Men", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[14]")},
            { "Games", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[15]")},
            { "Poker", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[16]")},
            { "Entertainment", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[17]")},

            { "Kyiv", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[18]")},
            { "Lviv", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[19]")},
            { "Odesa", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[20]")},
            { "Dnipro", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[21]")},
            { "Kharkiv", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[22]")},
            { "Radio", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[23]")},
            { "Movie", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[24]")},

            { "Lifestyle", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[25]")},
            { "Fashion", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[26]")},
            { "Pets", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[27]")},
            { "Baby", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[28]")},
            { "Love", By.XPath("//*[@id=\"subsite-top-menu\"]/div/div/div/div[1]/a[29]")},

        };

        private static Dictionary<string, By> _ukraineButtonWithDifferentsAxis = new Dictionary<string, By>()
        {

            { "FullPath", By.XPath("/html/body/app-root/app-default-layout/div[3]/app-main/app-default-main-page/section[1]/div/div/div[2]/app-top-tags/div/a[1]")},
            { "ChildAxis", By.XPath("//app-default-main-page/section[1]/div/div/div[2]/app-top-tags/div/a[1]")},
            { "DescendantAxis", By.XPath("//app-default-main-page/section[1]//div[2]/app-top-tags//a[1]")},
            { "ParentAxis", By.XPath("//app-default-main-page/section[1]//div[2]/app-top-tags//a[1]/.")},
            { "AncestorAxis", By.XPath("//app-default-main-page/section[1]//div[2]/app-top-tags//a[1]//ancestor::a[1]")},

        };

        public static Dictionary<string, By> Header { get { return _headerElements; } }
        public static Dictionary<string, By> CategoriesOfNewsPanel { get { return AllCategoriesOfNews; } }
        public static Dictionary<string, By> UkraineAxis { get { return _ukraineButtonWithDifferentsAxis; } }

    }
}
