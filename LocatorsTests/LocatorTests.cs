using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace LocatorsTests
{
    public class Tests
    {
        IWebDriver driver;
        WebDriverWait wait;
        const string url = "https://24tv.ua/";

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        }

        [TestCase("Logo")]
        [TestCase("BurgerMenu")]
        [TestCase("AllNews")]
        [TestCase("UkraineNews")]
        [TestCase("MainNews")]
        [TestCase("Fund24")]
        [TestCase("SearchIcon")]
        [TestCase("WatchOnline")]
        public void HeaderElementsFound(string selector)
        {
            driver.Url = url;
            var element = wait.Until(d => d.FindElement(Locators.Header[selector]));
            Assert.IsTrue(element.Displayed);
            driver.Quit();
        }



        [TestCase]
        public void AllCategoriesOfNewsElementsFound()
        {
         
            driver.Url = url;
            var element = wait.Until(d => d.FindElement(Locators.CategoriesOfNewsPanel["MoreLinks"]));
            element.Click();

            foreach (var category in Locators.CategoriesOfNewsPanel)
            {
                string categoryName = category.Key;
                By categoryXPath = category.Value;

                try
                {

                    element = wait.Until(d => d.FindElement(categoryXPath));

                    Assert.IsTrue(element.Displayed);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            driver.Quit();
        }

        [TestCase("FullPath", "a")]
        [TestCase("ChildAxis", "a")]
        [TestCase("DescendantAxis", "a")]
        [TestCase("ParentAxis", "a")]
        [TestCase("AncestorAxis", "a")]
        public void SearchSameButtonWithDifferentAxis(string xPath, string expectedTag)
        {
            driver.Url = url;

            var element = wait.Until(d => d.FindElement(Locators.UkraineAxis[xPath]));
        
            bool result = element.Displayed && element.TagName == expectedTag;
        
            Assert.IsTrue(result);
            driver.Quit();
        }
        
    }

}